var baseGPX = '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?><gpx version="1.1" creator="ethermap" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"></gpx>'

function addGPXpoint(base, lat, lon, name, desc, color) {
    var end = '</gpx>'
    var point = '<wpt lat="'+lat+'" lon="'+lon+'">'
    if (name) point += '<name>'+name+'</name>'
    if (desc) point += '<desc>'+desc+'</desc>'
    if (color) point += '<extensions><color>'+color+'</color></extensions>'
    point += '</wpt>'
    return base.replace(end, point+end)
}


var link = window.location.hash.slice(1)+'/export/txt'
fetch(link).then(function(response) {
    return response.text()
}).then(function(text) {
    console.log('Pad text length: ', text.length)
    var lines = text.split(/\r?\n/)
    var mapping = false
    var startTag = '--- map ---'
    var endTag = '--- /map ---'
    var re = new RegExp(startTag+'([\\s\\S]*)'+endTag, 'g')
    text = re.exec(text)[0]
    var blocks = text.match(/\n([\s\S]+?)(?=\n\n)/g)
    // var points = []

		var map = L.map('map')

		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		// L.tileLayer('http://{s}.tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/{z}/{x}/{y}.png', {
        id: 'mapbox.streets',
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' + '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="http://mapbox.com">Mapbox</a>',
		}).addTo(map);

		var Icone = L.Icon.extend({
			options: {
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			},
		});

		var icone_amarelo = new Icone({iconUrl: 'images/amarelo.png'})
		var icone_azul = new Icone({iconUrl: 'images/azul.png'})
		var icone_vermelho = new Icone({iconUrl: 'images/vermelho.png'})
		var icone_verde = new Icone({iconUrl: 'images/verde.png'})

    var count = 0
    var initial = [0,0]
    blocks.forEach(function(block) {
        var lines = block.trim().match(/[^\r\n]+/g)
        var pointText = lines[0].trim()
        var latlon = lines[1].split(',')
        var lat = parseFloat(latlon[0])
        var lon = parseFloat(latlon[1])
        if (initial[0] == 0) initial = [lat, lon]
        // points.push([lat, lon, pointText])

        var icone_usado = icone_azul
        var color = '#004444cc'
        if (pointText[0] == '$') {
            icone_usado = icone_amarelo
            color = '#0044cccc'
        } else if (pointText[0] == '!') {
            icone_usado = icone_vermelho
            color = '#00cc4444'
        } else if (pointText[0] == '.') {
            icone_usado = icone_azul
            color = '#004444cc'
        } else if (pointText[0] == '@') {
            icone_usado = icone_verde
            color = '#0044cc44'
        }

        L.marker([lat, lon], {icon: icone_usado}).addTo(
            map).bindPopup(pointText).openPopup()

        var index = pointText.indexOf('(')
        var name = pointText
        var desc = null
        if (index >= 0) {
            name = pointText.slice(0, index)
            desc = pointText.slice(index)
        }
        baseGPX = addGPXpoint(baseGPX, lat, lon, name, desc, color)
        count += 1
    })

    console.log('Number of points:', count)

    map.setView(initial, 13)

		var popup = L.popup()
		function onMapClick(e) {
			popup
				.setLatLng(e.latlng)
				.setContent("You clicked the map at " + e.latlng.toString())
				.openOn(map);
		}
		map.on('click', onMapClick);

    // GPX export
    var link = document.querySelector('#download-gpx')
    var encodedUri = encodeURI('data:text/gpx;charset=utf-8,' + baseGPX)
    encodedUri = encodedUri.replace(/#/g, '%23')
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "favourites.gpx");
})
