# EtherMap

Display maps described etherpad.

## Example

Place a text like this in an etherpad:

    --- map ---

    description1
    10.20, 0.11

    description2
    20.50, 30.0

    --- /map ---
  
Open ethermap with the pad link after the `#`:

http://andresmrm.gitlab.io/ethermap/#https://pad.okfn.org/p/ethermap-example

Click points to see description.


## Import points to osmand

Click the `gpx` link (upper right) to download the points as a gpx file.
Replace osmand `favourites.gpx` with this file. For me this command works:

** WARNING: you may lose your osmand favourites **

    mv /sdcard/Download/favourites.gpx /storage/emulated/0/Android/data/net.osmand.plus/files/
